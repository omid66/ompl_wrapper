#include <ompl_wrapper/rrt_base.h>
class IKGoal : public ompl::base::Goal
{
public:
  IKGoal(const ob::SpaceInformationPtr &si) : ompl::base::Goal(si)
  {
  }

  bool setup_planner(manipulator_kdl::robotKDL robot_kdl);
  bool setup_goal(Eigen::VectorXd g_pt);
  manipulator_kdl::robotKDL robot_kdl_;
  Eigen::VectorXd goal_pt;
  int dim=0;
  double thresh=0.1;
  
  virtual bool isSatisfied(const ob::State *st) const;

// this function computes the distance to the desired pose:
  virtual bool isSatisfied(const ob::State *st, double *distance) const;
};


// state validity checker class:
class myStateValidityCheckerClass : public ob::StateValidityChecker
{
public:
  double thresh=0.01;
  myStateValidityCheckerClass(const ob::SpaceInformationPtr &si) : ob::StateValidityChecker(si){}
  // check for validity, bounds are already set, so only check for collisions?
  virtual bool isValid(const ob::State *state) const;
};

namespace ompl_wrapper
{
class ikProblem : public IKompl
{
public:
  void setupGoal(Eigen::VectorXd goal_pt);
};
}
