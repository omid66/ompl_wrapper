#include <ros/ros.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/base/goals/GoalLazySamples.h>
#include <ompl/geometric/GeneticSearch.h>

#include <ompl/config.h>
#include <iostream>

#include <ll4ma_kdl/manipulator_kdl/robot_kdl.h>
namespace ob = ompl::base;
namespace og = ompl::geometric;


using namespace std;

namespace ompl_wrapper
{
class IKompl
{

public:
//IKompl(){};
bool init_planner(manipulator_kdl::robotKDL robot_model);

bool solve(const double &time, Eigen::VectorXd start_state);

vector<Eigen::VectorXd> q_solution;
protected:

manipulator_kdl::robotKDL robot_kdl_;

// ompl variables:
ob::RealVectorStateSpace* js;
ob::StateSpacePtr space;//(js);
og::SimpleSetup* ss;//(space);

};
}
