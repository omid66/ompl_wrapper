#include <ompl_wrapper/example_problem.h>
using namespace ompl_wrapper;
int main(int argc, char ** argv)
{
    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;
    ros::init(argc,argv,"kdl_test_node");
    ros::NodeHandle n;

    // initialize wrapper:
    ikProblem ik_prob;
    // read robot kdl:
    vector<string> ee_names={"lbr4_7_link"};
    vector<string> base_names={"base_link"};
    string n_name="robot_description";
    vector<double> g_vec={0.0,0.0,-9.8};

    manipulator_kdl::robotKDL robot_kdl(n_name,n,ee_names,base_names,g_vec);
    robot_kdl.urdfParser();

    ik_prob.init_planner(robot_kdl);
    Eigen::VectorXd g_pt(3);
    g_pt.setZero();
    g_pt[0]=0.4;
    g_pt[1]=0.4;
    g_pt[2]=0.2;
    ik_prob.setupGoal(g_pt);

    // solve:
    Eigen::VectorXd start_state(7);
    start_state.setZero();
    ik_prob.solve(10.0,start_state);
    cerr<<ik_prob.q_solution[ik_prob.q_solution.size()-1].transpose()<<endl;
}
