#include <ros/ros.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/base/goals/GoalLazySamples.h>
#include <ompl/geometric/GeneticSearch.h>

#include <ompl/config.h>
#include <iostream>

#include <ll4ma_kdl/manipulator_kdl/robot_kdl.h>
namespace ob = ompl::base;
namespace og = ompl::geometric;

using namespace std;


class IKGoal : public ompl::base::Goal
{
public:
IKGoal(const ob::SpaceInformationPtr &si) : ompl::base::Goal(si)
{
}

  bool setup_IK(ros::NodeHandle &n,string &robot_description, vector<string> ee_names, vector<string> base_names,Eigen::VectorXd &g_pt)
{
  vector<double> g_vec={0.0,0.0,-9.8};
  manipulator_kdl::robotKDL lbr4_(robot_description,n, ee_names, base_names,g_vec);
  robot_kdl=lbr4_;
  dim=7;
  goal_pt=g_pt;
  return true;
}
manipulator_kdl::robotKDL robot_kdl;
  Eigen::VectorXd goal_pt;
int dim=0;
double thresh=0.1;
  
virtual bool isSatisfied(const ob::State *st) const
{
  // get ik
  Eigen::VectorXd q_state(dim);
  for(int i=0;i<dim;++i)
  {
    q_state[i]=st->as<ob::RealVectorStateSpace::StateType>()->values[i];
  }
  Eigen::VectorXd ee_pose;
  robot_kdl.getFK(0,q_state,ee_pose);

  double d=(goal_pt-ee_pose.head(3)).squaredNorm();
  if(d<0.001)
  {
    return true;
  }
  else
  {
    return false;
  }
}

// this function computes the distance to the desired pose:
virtual bool isSatisfied(const ob::State *st, double *distance) const
{
  Eigen::VectorXd q_state(dim);
  for(int i=0;i<dim;++i)
  {
    q_state[i]=st->as<ob::RealVectorStateSpace::StateType>()->values[i];
  }
  Eigen::VectorXd ee_pose;
  robot_kdl.getFK(0,q_state,ee_pose);

  double d=(goal_pt-ee_pose.head(3)).squaredNorm();
  *distance=d;
  if(d<0.001)
  {
    return true;
  }
  else
  {
    return false;
  }

}
};


// state validity checker class:
class myStateValidityCheckerClass : public ob::StateValidityChecker
{
public:
  double thresh=0.01;
  myStateValidityCheckerClass(const ob::SpaceInformationPtr &si) :
    ob::StateValidityChecker(si)
  {
  }

  // check for validity, bounds are already set, so only check for collisions?
  virtual bool isValid(const ob::State *state) const
  {
    //double* data=state->as<ob::RealVectorStateSpace::StateType>()->values;
    return true;
  }
};

int main(int argc, char ** argv)
{
    std::cout << "OMPL version: " << OMPL_VERSION << std::endl;
    ros::init(argc,argv,"kdl_test_node");
    ros::NodeHandle n;

    // state space of 7 joints:
    ob::RealVectorStateSpace* r7(new ompl::base::RealVectorStateSpace(7));
    ob::RealVectorBounds bounds(7);
    bounds.setLow(-2.5);
    bounds.setHigh(2.5);
    r7->setBounds(bounds);

    ob::StateSpacePtr space(r7);
    og::SimpleSetup ss(space);

    IKGoal* goal(new IKGoal(ss.getSpaceInformation()));
    ss.setStateValidityChecker(make_shared<myStateValidityCheckerClass>(ss.getSpaceInformation()));

    std::vector<double> js={0.0,0.0,0.0,0.0,0.0,0.0,0.0};

    // random start state:
    ob::ScopedState<ob::RealVectorStateSpace> start(space);

    
    // assign values:
    for(int i=0;i<js.size();++i)
    {
      start[i]=js[i];
    }
    ss.addStartState(start);

    //ob::SpaceInformationPtr si(space);
    // either this call:
    //ss.setStateValidityCheckingResolution(0.03); // 3%
    //ss.setup();
    
    vector<string> ee_names={"lbr4_7_link"};
    vector<string> base_names={"base_link"};
    string n_name="robot_description";
    Eigen::VectorXd g_pt(3);
    g_pt.setZero();
    g_pt[0]=0.4;
    g_pt[1]=0.4;
    g_pt[2]=0.3;
    goal->setup_IK(n,n_name,base_names,ee_names,g_pt);
    ob::GoalPtr goal_ptr(goal);
    ss.setGoal(goal_ptr);
    



    ob::PlannerStatus solved = ss.solve(10.0);

    if (solved)
    {
        std::cout << "Found solution:" << std::endl;
        // print the path to screen
        ss.simplifySolution();
        int len=ss.getSolutionPath().getStateCount();
        vector<Eigen::VectorXd> path;
        path.resize(len);
        
        for(int i=0;i<len;++i)
        {
          
          double* s=ss.getSolutionPath().getState(i)->as<ob::RealVectorStateSpace::StateType>()->values;
          path[i].resize(7);
          for(int j=0;j<7;++j)
          {
            path[i][j]=s[j];
          }
          //cerr<<path[i].transpose()<<endl;
          Eigen::VectorXd ee_pose;
          goal->robot_kdl.getFK(0,path[i],ee_pose);
          cerr<<(g_pt-ee_pose.head(3)).squaredNorm()<<endl;

        }
    }
    else
        std::cout << "No solution found" << std::endl;

    return 0;
}
