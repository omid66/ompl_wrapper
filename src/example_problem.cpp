#include <ompl_wrapper/example_problem.h>


bool IKGoal::setup_planner(manipulator_kdl::robotKDL robot_kdl)
{
  robot_kdl_=robot_kdl;
  dim=robot_kdl_.dof;
  return true;
}
bool IKGoal::setup_goal(Eigen::VectorXd g_pt)
{
  goal_pt=g_pt;
}
bool IKGoal::isSatisfied(const ob::State *st) const
{
  // get ik
  Eigen::VectorXd q_state(dim);
  for(int i=0;i<dim;++i)
  {
    q_state[i]=st->as<ob::RealVectorStateSpace::StateType>()->values[i];
  }
  Eigen::VectorXd ee_pose;
  robot_kdl_.getFK(0,q_state,ee_pose);

  double d=(goal_pt-ee_pose.head(3)).squaredNorm();
  if(d<0.001)
  {
    return true;
  }
  else
  {
    return false;
  }
}

// this function computes the distance to the desired pose:
bool IKGoal::isSatisfied(const ob::State *st, double *distance) const
{
  Eigen::VectorXd q_state(dim);
  for(int i=0;i<dim;++i)
  {
    q_state[i]=st->as<ob::RealVectorStateSpace::StateType>()->values[i];
  }
  Eigen::VectorXd ee_pose;
  robot_kdl_.getFK(0,q_state,ee_pose);

  double d=(goal_pt-ee_pose.head(3)).squaredNorm();
  *distance=d;
  if(d<0.001)
  {
    return true;
  }
  else
  {
    return false;
  }
}



// check for validity, bounds are already set, so only check for collisions?
bool myStateValidityCheckerClass::isValid(const ob::State *state) const
{
  //double* data=state->as<ob::RealVectorStateSpace::StateType>()->values;
  return true;
}

namespace ompl_wrapper
{
void ikProblem::setupGoal(Eigen::VectorXd goal_pt)
{
  // setup goal and state validity classes:
  IKGoal* goal(new IKGoal(ss->getSpaceInformation()));
  ss->setStateValidityChecker(make_shared<myStateValidityCheckerClass>(ss->getSpaceInformation()));
  goal->setup_planner(robot_kdl_);
  goal->setup_goal(goal_pt);
  ob::GoalPtr goal_ptr(goal);
  ss->setGoal(goal_ptr);
}
}
