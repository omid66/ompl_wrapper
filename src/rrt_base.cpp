#include <ompl_wrapper/rrt_base.h>

namespace ompl_wrapper
{
bool IKompl::init_planner(manipulator_kdl::robotKDL robot_model)
{
  robot_kdl_=robot_model;

  // set joint space dof:
  // state space of 7 joints:
  js=new ompl::base::RealVectorStateSpace(robot_model.dof);
  
  ob::RealVectorBounds bounds(robot_model.dof);
  bounds.low=robot_kdl_.low_bounds;
  bounds.high=robot_kdl_.up_bounds;
  js->setBounds(bounds);
  
  space=ob::StateSpacePtr(js);
  ss=new og::SimpleSetup(space);
  
  return true;
}

bool IKompl::solve(const double &time, Eigen::VectorXd start_state)
{
  
  ob::ScopedState<ob::RealVectorStateSpace> start(space);
  
  // assign values:
  for(int i=0;i<start_state.size();++i)
  {
    start[i]=start_state[i];
  }
  ss->addStartState(start);

  ob::PlannerStatus solved = ss->solve(10.0);
  if (solved)
  {
    std::cout << "Found solution:" << std::endl;
    // print the path to screen
    ss->simplifySolution();
    int len=ss->getSolutionPath().getStateCount();
    vector<Eigen::VectorXd> path;
    path.resize(len);
    
    for(int i=0;i<len;++i)
    {
      
      double* s=ss->getSolutionPath().getState(i)->as<ob::RealVectorStateSpace::StateType>()->values;
      path[i].resize(7);
      for(int j=0;j<7;++j)
      {
        path[i][j]=s[j];
      }
    }
    q_solution=path;
    return true;
  }
  else
  {
    std::cout << "No solution found" << std::endl;
    return false;
  }   


}

}
